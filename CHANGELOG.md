# Changelog

## 3.0.1 (August 2, 2020)

* Fixed: Author info

## 3.0.0 (August 2, 2020)

* First official release as a standalone library
