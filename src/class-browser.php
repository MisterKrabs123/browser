<?php
/**
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files ( the "Software"),
 * to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Originally by Chris Schuld (https://chrisschuld.com/)
 * Modified by Chris Christoff
 * Maintained by Widgit Labs
 *
 * This fork strives to keep the functionality of his version, while
 * maintaining compliance with PHPCS and the WPCS and WidgitWPCS coding
 * standards. By extension, WidgitWPCS compliance should also come very
 * close to covering VIPWPCS standards.
 *
 * Typical Usage:
 *
 *   $browser = new Browser();
 *   if( $browser->get_browser() == Browser::BROWSER_FIREFOX && $browser->get_version() >= 10 ) {
 *       echo 'You have FireFox version 10 or greater';
 *   }
 *
 * This implementation is based on the original work from Gary White
 * https://apptools.com/phptools/browser/
 *
 * @package     Widgit\PHP\Browser
 * @version     3.0.1
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Class for determining browser information
 *
 * @since       1.0.0
 */
class Browser {


	/**
	 * The user agent
	 *
	 * @access      private
	 * @since       1.0.0
	 * @var         string $agent The user agent
	 */
	private $agent = '';


	/**
	 * The browser name
	 *
	 * @access      private
	 * @since       1.0.0
	 * @var         string $browser_name The browser name
	 */
	private $browser_name = '';


	/**
	 * The browser version
	 *
	 * @access      private
	 * @since       1.0.0
	 * @var         string $version The browser version
	 */
	private $version = '';


	/**
	 * The users platform
	 *
	 * @access      private
	 * @since       1.0.0
	 * @var         string $platform The users platform
	 */
	private $platform = '';


	/**
	 * The users OS
	 *
	 * @access      private
	 * @since       1.0.0
	 * @var         string $os The users OS
	 */
	private $os = '';


	/**
	 * Whether or not the user is using AOL
	 *
	 * @access      private
	 * @since       1.0.0
	 * @var         bool $is_aol Whether or not the user is using AOL
	 */
	private $is_aol = false;


	/**
	 * Whether or not the user is using a mobile phone
	 *
	 * @access      private
	 * @since       1.0.0
	 * @var         bool $is_mobile Whether or not the user is using a mobile device
	 */
	private $is_mobile = false;


	/**
	 * Whether or not the user is using a tablet
	 *
	 * @access      private
	 * @since       3.0.0
	 * @var         bool $is_tablet Whether or not the user is using a tablet
	 */
	private $is_tablet = false;


	/**
	 * Whether or not the user is a bot
	 *
	 * @access      private
	 * @since       3.0.0
	 * @var         bool $is_bot Whether or not the user is a bot
	 */
	private $is_bot = false;


	/**
	 * Whether or not the browser is from Facebook
	 *
	 * @access      private
	 * @since       3.0.0
	 * @var         bool $is_facebook Whether or not the browser is from Facebook
	 */
	private $is_facebook = false;


	/**
	 * The AOL version
	 *
	 * @access      private
	 * @since       1.0.0
	 * @var         string $aol_version The AOL version
	 */
	private $aol_version = '';


	/**
	 * The string for unknown browsers
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_UNKNOWN The string for unknown browsers
	 */
	public const BROWSER_UNKNOWN = 'unknown';


	/**
	 * The string for Amaya
	 * https://www.w3.org/Amaya/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_AMAYA The string for Amaya
	 */
	public const BROWSER_AMAYA = 'Amaya';


	/**
	 * The string for Android
	 * https://www.android.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_ANDROID The string for Android
	 */
	public const BROWSER_ANDROID = 'Android';


	/**
	 * The string for Bing Bots
	 * https://wikipedia.org/wiki/Bingbot
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_BING_BOT The string for Bing Bots
	 */
	public const BROWSER_BING_BOT = 'Bing Bot';


	/**
	 * The string for BlackBerry
	 * https://wikipedia.org/wiki/BlackBerry_Browser
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_BLACKBERRY The string for BlackBerry
	 */
	public const BROWSER_BLACKBERRY = 'BlackBerry';


	/**
	 * The string for Brave
	 * https://brave.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_BRAVE The string for Brave
	 */
	public const BROWSER_BRAVE = 'Brave';


	/**
	 * The string for Chrome
	 * https://www.google.com/chrome/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_CHROME The string for Chrome
	 */
	public const BROWSER_CHROME = 'Chrome';


	/**
	 * The string for Cocoa Rest Client
	 * https://mmattozzi.github.io/cocoa-rest-client/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_COCOA The string for Cocoa Rest Client
	 */
	public const BROWSER_COCOA = 'Cocoa Rest Client';


	/**
	 * The string for cURL
	 * https://curl.haxx.se/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_CURL The string for cURL
	 */
	public const BROWSER_CURL = 'cURL';


	/**
	 * The string for Edge
	 * https://www.microsoft.com/edge/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_EDGE The string for Edge
	 */
	public const BROWSER_EDGE = 'Edge';


	/**
	 * The string for Firebird
	 * https://www.ibphoenix.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_FIREBIRD The string for Firebird
	 */
	public const BROWSER_FIREBIRD = 'Firebird';


	/**
	 * The string for Firefox
	 * https://www.mozilla.org/en-US/firefox/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_FIREFOX The string for Firefox
	 */
	public const BROWSER_FIREFOX = 'Firefox';


	/**
	 * The string for Galeon (DEPRECATED)
	 * https://galeon.sourceforge.net/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_GALEON The string for Galeon
	 */
	public const BROWSER_GALEON = 'Galeon';


	/**
	 * The string for GoogleBot
	 * https://www.google.com/support/webmasters/bin/answer.py?answer=80553
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_GOOGLEBOT The string for GoogleBot
	 */
	public const BROWSER_GOOGLEBOT = 'GoogleBot';


	/**
	 * The string for iCab
	 * https://www.icab.de/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string $browser_icab The string for iCab
	 */
	public const BROWSER_ICAB = 'iCab';


	/**
	 * The string for IceCat
	 * https://www.gnu.org/software/gnuzilla/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_ICECAT The string for IceCat
	 */
	public const BROWSER_ICECAT = 'IceCat';


	/**
	 * The string for Iceweasel (DEPRECATED)
	 * https://wikipedia.org/wiki/Mozilla_software_rebranded_by_Debian#Iceweasel
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_ICEWEASEL The string for Iceweasel
	 */
	public const BROWSER_ICEWEASEL = 'Iceweasel';


	/**
	 * The string for Ineternet Explorer
	 * https://www.microsoft.com/ie/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_INTERNET_EXPLORER The string for Internet Explorer
	 */
	public const BROWSER_INTERNET_EXPLORER = 'Internet Explorer';


	/**
	 * The string for Iframely
	 * https://iframely.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_IFRAMELY The string for Iframely
	 */
	public const BROWSER_IFRAMELY = 'Iframely';


	/**
	 * The string for iPad
	 * https://www.apple.com/ipad/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_IPAD The string for iPad
	 */
	public const BROWSER_IPAD = 'iPad';


	/**
	 * The string for iPhone
	 * https://www.apple.com/iphone/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_IPHONE The string for iPhone
	 */
	public const BROWSER_IPHONE = 'iPhone';


	/**
	 * The string for iPod
	 * https://www.apple.com/ipod-touch/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_IPOD The string for iPod
	 */
	public const BROWSER_IPOD = 'iPod';


	/**
	 * The string for Konqueror
	 * https://kde.org/applications/internet/org.kde.konqueror/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_KONQUEROR The string for Konqueror
	 */
	public const BROWSER_KONQUEROR = 'Konqueror';


	/**
	 * The string for Lynx
	 * https://invisible-island.net/lynx/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_LYNX The string for Lynx
	 */
	public const BROWSER_LYNX = 'Lynx';


	/**
	 * The string for Mozilla
	 * https://www.mozilla.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_MOZILLA The string for Mozilla
	 */
	public const BROWSER_MOZILLA = 'Mozilla';


	/**
	 * The string for MSN
	 * https://explorer.msn.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_MSN The string for MSN
	 */
	public const BROWSER_MSN = 'MSN Browser';


	/**
	 * The string for MSN Bots
	 * https://search.msn.com/msnbot.htm
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_MSN_BOT The string for MSN Bots
	 */
	public const BROWSER_MSN_BOT = 'MSN Bot';


	/**
	 * The string for NetPositive (DEPRECATED)
	 * https://wikipedia.org/wiki/NetPositive
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_NETPOSITIVE The string for NetPositive
	 */
	public const BROWSER_NETPOSITIVE = 'NetPositive';


	/**
	 * The string for Netscape Navigator (DEPRECATED)
	 * https://browser.netscape.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_NETSCAPE_NAVIGATOR The string for Netscape Navigator
	 */
	public const BROWSER_NETSCAPE_NAVIGATOR = 'Netscape Navigator';


	/**
	 * The string for Nokia
	 * https://www.nokia.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_NOKIA The string for Nokia
	 */
	public const BROWSER_NOKIA = 'Nokia Browser';


	/**
	 * The string for Nokia S60 (DEPRECATED)
	 * https://wikipedia.org/wiki/Web_Browser_for_S60
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_NOKIA_S60 The string for Nokia S60
	 */
	public const BROWSER_NOKIA_S60 = 'Nokia S60 OSS Browser';


	/**
	 * The string for OmniWeb
	 * https://www.omnigroup.com/applications/omniweb/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_OMNIWEB The string for OmniWeb
	 */
	public const BROWSER_OMNIWEB = 'OmniWeb';


	/**
	 * The string for Opera
	 * https://www.opera.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_OPERA The string for Opera
	 */
	public const BROWSER_OPERA = 'Opera';


	/**
	 * The string for Opera mini
	 * https://www.opera.com/mini/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_OPERA_MINI The string for Opera
	 */
	public const BROWSER_OPERA_MINI = 'Opera Mini';


	/**
	 * The string for Palemoon
	 * https://www.palemoon.org/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_PALEMOON The string for Palemoon
	 */
	public const BROWSER_PALEMOON = 'Palemoon';


	/**
	 * The string for Phoenix (DEPRECATED)
	 * https://wikipedia.org/wiki/History_of_Mozilla_Firefox
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_PHOENIX The string for Phoenix
	 */
	public const BROWSER_PHOENIX = 'Phoenix';


	/**
	 * The string for Pocket IE (DEPRECATED)
	 * https://wikipedia.org/wiki/Internet_Explorer_Mobile
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_POCKET_IE The string for Pocket IE
	 */
	public const BROWSER_POCKET_IE = 'Pocket Internet Explorer';


	/**
	 * The string for Safari
	 * https://www.apple.com/safari/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_SAFARI The string for Safari
	 */
	public const BROWSER_SAFARI = 'Safari';


	/**
	 * The string for Samsung Internet
	 * https://www.samsung.com/us/support/owners/app/samsung-internet/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_SAMSUNG_INTERNET The string for Samsung Internet
	 */
	public const BROWSER_SAMSUNG_INTERNET = 'Samsung Internet';


	/**
	 * The string for Shiretoko
	 * https://wiki.mozilla.org/Projects/shiretoko/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_SHIRETOKO The string for Shiretoko
	 */
	public const BROWSER_SHIRETOKO = 'Shiretoko';


	/**
	 * The string for Silk
	 * https://docs.aws.amazon.com/silk/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_SILK The string for Silk
	 */
	public const BROWSER_SILK = 'Silk';


	/**
	 * The string for Slurp
	 * https://www.botreports.com/y/yahoo-slurp.shtml
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_SLURP The string for Slurp
	 */
	public const BROWSER_SLURP = 'Yahoo! Slurp';


	/**
	 * The string for UCBrowser
	 * https://www.ucweb.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_UCBROWSER The string for UCBrowser
	 */
	public const BROWSER_UCBROWSER = 'UCBrowser';


	/**
	 * The string for Vivaldi
	 * https://vivaldi.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_VIVALDI The string for Vivaldi
	 */
	public const BROWSER_VIVALDI = 'Vivaldi';


	/**
	 * The string for W3C Validator
	 * https://validator.w3.org/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_W3C_VALIDATOR The string for W3C Validator
	 */
	public const BROWSER_W3C_VALIDATOR = 'W3C Validator';


	/**
	 * The string for WebTV (DEPRECATED)
	 * https://wikipedia.org/wiki/MSN_TV
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_WEBTV The string for WebTV
	 */
	public const BROWSER_WEBTV = 'WebTV';


	/**
	 * The string for Wget
	 * https://www.gnu.org/software/wget/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_WGET The string for Wget
	 */
	public const BROWSER_WGET = 'Wget';


	/**
	 * The string for Yandex
	 * https://browser.yandex.ua/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_YANDEX The string for Yandex
	 */
	public const BROWSER_YANDEX = 'Yandex';


	/**
	 * The string for Yandex Bots
	 * https://yandex.com/bots/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_YANDEX_BOT The string for Yandex Bots
	 */
	public const BROWSER_YANDEX_BOT = 'Yandex Bot';


	/**
	 * The string for Yandex Blogs
	 * https://yandex.com/bots/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_YANDEX_BOT_BLOGS The string for Yandex Blogs
	 */
	public const BROWSER_YANDEX_BOT_BLOGS = 'Yandex Blogs';


	/**
	 * The string for Yandex Catalog
	 * https://yandex.com/bots/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_YANDEX_BOT_CATALOG The string for Yandex Catalog
	 */
	public const BROWSER_YANDEX_BOT_CATALOG = 'Yandex Catalog';


	/**
	 * The string for Yandex Direct
	 * https://yandex.com/bots/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_YANDEX_BOT_DIRECT The string for Yandex Direct
	 */
	public const BROWSER_YANDEX_BOT_DIRECT = 'Yandex Direct';


	/**
	 * The string for Yandex Favicons
	 * https://yandex.com/bots/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_YANDEX_BOT_FAVICONS The string for Yandex Favicons
	 */
	public const BROWSER_YANDEX_BOT_FAVICONS = 'Yandex Favicons';


	/**
	 * The string for Yandex Image Resizer
	 * https://yandex.com/bots/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_YANDEX_BOT_IMAGERESIZER The string for Yandex Image Resizer
	 */
	public const BROWSER_YANDEX_BOT_IMAGERESIZER = 'Yandex Image Resizer';


	/**
	 * The string for Yandex Images
	 * https://yandex.com/bots/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_YANDEX_BOT_IMAGES The string for Yandex Images
	 */
	public const BROWSER_YANDEX_BOT_IMAGES = 'Yandex Images';


	/**
	 * The string for Yandex Media
	 * https://yandex.com/bots/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_YANDEX_BOT_MEDIA The string for Yandex Media
	 */
	public const BROWSER_YANDEX_BOT_MEDIA = 'Yandex Media';


	/**
	 * The string for Yandex Metrika
	 * https://yandex.com/bots/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_YANDEX_BOT_METRIKA The string for Yandex Metrika
	 */
	public const BROWSER_YANDEX_BOT_METRIKA = 'Yandex Metrika';


	/**
	 * The string for Yandex News
	 * https://yandex.com/bots/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_YANDEX_BOT_NEWS The string for Yandex News
	 */
	public const BROWSER_YANDEX_BOT_NEWS = 'Yandex News';


	/**
	 * The string for Yandex Webmaster
	 * https://yandex.com/bots/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_YANDEX_BOT_WEBMASTER The string for Yandex Webmaster
	 */
	public const BROWSER_YANDEX_BOT_WEBMASTER = 'Yandex Webmaster';


	/**
	 * The string for Yandex Video
	 * https://yandex.com/bots/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string BROWSER_YANDEX_BOT_VIDEO The string for Yandex Video
	 */
	public const BROWSER_YANDEX_BOT_VIDEO = 'Yandex Video';


	/**
	 * The string for unknown platforms
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_UNKNOWN The string for unknown platforms
	 */
	public const PLATFORM_UNKNOWN = 'unknown';


	/**
	 * The string for Android
	 * https://www.android.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_ANDROID The string for Android
	 */
	public const PLATFORM_ANDROID = 'Android';


	/**
	 * The string for Apple
	 * https://www.apple.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_APPLE The string for Apple
	 */
	public const PLATFORM_APPLE = 'Apple';


	/**
	 * The string for Apple TV
	 * https://www.apple.com/tv/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_APPLE_TV The string for Apple TV
	 */
	public const PLATFORM_APPLE_TV = 'Apple TV';


	/**
	 * The string for BeOS (DEPRECATED)
	 * https://wikipedia.org/wiki/BeOS
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_BEOS The string for BeOS
	 */
	public const PLATFORM_BEOS = 'BeOS';


	/**
	 * The string for BlackBerry
	 * https://www.blackberry.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_BLACKBERRY The string for BlackBerry
	 */
	public const PLATFORM_BLACKBERRY = 'BlackBerry';


	/**
	 * The string for Chrome OS
	 * https://www.google.com/chromebook/chrome-os/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_CHROME_OS The string for Chrome OS
	 */
	public const PLATFORM_CHROME_OS = 'Chrome OS';


	/**
	 * The string for Fire OS
	 * http://developer.amazon.com/docs/fire-tv/fire-os-overview.html
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_FIRE_OS The string for Fire OS
	 */
	public const PLATFORM_FIRE_OS = 'Fire OS';


	/**
	 * The string for FreeBSD
	 * https://www.freebsd.org/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_FREEBSD The string for FreeBSD
	 */
	public const PLATFORM_FREEBSD = 'FreeBSD';


	/**
	 * The string for Iframely
	 * https://iframely.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_IFRAMELY The string for Iframely
	 */
	public const PLATFORM_IFRAMELY = 'Iframely';


	/**
	 * The string for iPad
	 * https://www.apple.com/ipad-touch/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_IPAD The string for iPad
	 */
	public const PLATFORM_IPAD = 'iPad';


	/**
	 * The string for iPhone
	 * https://www.apple.com/iphone/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_IPHONE The string for iPhone
	 */
	public const PLATFORM_IPHONE = 'iPhone';


	/**
	 * The string for iPod
	 * https://www.apple.com/ipod/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_IPOD The string for iPod
	 */
	public const PLATFORM_IPOD = 'iPod';


	/**
	 * The string for Java/Android
	 * https://www.android.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_JAVA_ANDROID The string for Java/Android
	 */
	public const PLATFORM_JAVA_ANDROID = 'Java/Android';


	/**
	 * The string for Linux
	 * https://www.linux.org/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_LINUX The string for Linux
	 */
	public const PLATFORM_LINUX = 'Linux';


	/**
	 * The string for NetBSD
	 * https://www.netbsd.org/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_NETBSD The string for NetBSD
	 */
	public const PLATFORM_NETBSD = 'NetBSD';


	/**
	 * The string for Nokia
	 * https://www.nokia.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_NOKIA The string for Nokia
	 */
	public const PLATFORM_NOKIA = 'Nokia';


	/**
	 * The string for OpenBSD
	 * https://www.openbsd.org/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_OPENBSD The string for OpenBSD
	 */
	public const PLATFORM_OPENBSD = 'OpenBSD';


	/**
	 * The string for OpenSolaris (DEPRECATED)
	 * https://wikipedia.org/wiki/OpenSolaris
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_OPENSOLARIS The string for OpenSolaris
	 */
	public const PLATFORM_OPENSOLARIS = 'OpenSolaris';


	/**
	 * The string for OS/2 (DEPRECATED)
	 * https://wikipedia.org/wiki/OS/2
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_OS2 The string for OS/2
	 */
	public const PLATFORM_OS2 = 'OS/2';


	/**
	 * The string for PlayStation
	 * https://www.playstation.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_PLAYSTATION The string for PlayStation
	 */
	public const PLATFORM_PLAYSTATION = 'Sony PlayStation';


	/**
	 * The string for Postman
	 * https://www.postman.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_POSTMAN The string for Postman
	 */
	public const PLATFORM_POSTMAN = 'Postman';


	/**
	 * The string for Roku
	 * https://www.roku.com/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_ROKU The string for Roku
	 */
	public const PLATFORM_ROKU = 'Roku';


	/**
	 * The string for SMART-TV
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_SMART_TV The string for SMART-TV
	 */
	public const PLATFORM_SMART_TV = 'SMART-TV';


	/**
	 * The string for SunOS (DEPRECATED)
	 * https://wikipedia.org/wiki/SunOS
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_SUNOS The string for SunOS
	 */
	public const PLATFORM_SUNOS = 'SunOS';


	/**
	 * The string for Terminals (cURL/Wget)
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_TERMINAL The string for Terminals
	 */
	public const PLATFORM_TERMINAL = 'Terminal';


	/**
	 * The string for Windows
	 * https://www.microsoft.com/en-us/Windows/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_WINDOWS The string for Windows
	 */
	public const PLATFORM_WINDOWS = 'Windows';


	/**
	 * The string for Windows CE
	 * https://msdn.microsoft.com/en-ph/embedded/
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string PLATFORM_WINDOWS_CE The string for Windows CE
	 */
	public const PLATFORM_WINDOWS_CE = 'Windows CE';


	/**
	 * The string for unknown operating systems
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string OPERATING_SYSTEM_UNKNOWN The string for unknown operating systems
	 */
	public const OPERATING_SYSTEM_UNKNOWN = 'unknown';


	/**
	 * The string for unknown versions
	 *
	 * @access      public
	 * @since       3.0.0
	 * @var         string VERSION_UNKNOWN The string for unknown versions
	 */
	public const VERSION_UNKNOWN = 'unknown';


	/**
	 * Get things started
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       string $useragent The useragent to parse.
	 * @return      void
	 */
	public function __construct( $useragent = '' ) {
		$this->reset();

		if ( '' !== $useragent ) {
			$this->set_useragent( $useragent );
		} else {
			$this->determine();
		}
	}


	/**
	 * Reset all properties
	 *
	 * While the VIPWPCS sniff we are ignoring is valid, the point
	 * of this library is to fix the cases missed by HTTP_USER_AGENT.
	 * As such, we are using it as a baseline only.
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function reset() {
		$this->agent        = isset( $_SERVER['HTTP_USER_AGENT'] ) ? filter_var( $_SERVER['HTTP_USER_AGENT'], FILTER_SANITIZE_STRING ) : '';  // phpcs:ignore WordPressVIPMinimum.Variables.RestrictedVariables
		$this->browser_name = self::BROWSER_UNKNOWN;
		$this->version      = self::VERSION_UNKNOWN;
		$this->platform     = self::PLATFORM_UNKNOWN;
		$this->os           = self::OPERATING_SYSTEM_UNKNOWN;
		$this->is_aol       = false;
		$this->is_facebook  = false;
		$this->is_mobile    = false;
		$this->is_robot     = false;
		$this->is_tablet    = false;
		$this->aol_version  = self::VERSION_UNKNOWN;
	}


	/**
	 * Check to see if the specified browser matches the detected browser
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       string $browser_name The browser name to check.
	 * @return      bool Whether or not the browser is the specified browser
	 */
	public function is_browser( $browser_name ) {
		return ( 0 === strcasecmp( $this->browser_name, trim( $browser_name ) ) );
	}


	/**
	 * The name of the browser
	 * All return types are from the class constants
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      string $this->browser_name Name of the browser
	 */
	public function get_browser() {
		return $this->browser_name;
	}


	/**
	 * Set the name of the browser
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @param       string $browser The name of the browser.
	 * @return      void
	 */
	protected function set_browser( $browser ) {
		$this->browser_name = $browser;
	}


	/**
	 * The name of the platform
	 * All return types are from the class constants
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      string $this->platform The name of the browser
	 */
	public function get_platform() {
		return $this->platform;
	}


	/**
	 * Set the name of the platform
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @param       string $platform The name of the platform.
	 * @return      void
	 */
	protected function set_platform( $platform ) {
		$this->platform = $platform;
	}


	/**
	 * The version of the browser
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      string $this->version The version of the browser
	 */
	public function get_version() {
		return $this->version;
	}


	/**
	 * Set the version of the browser
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @param       string $version The version of the browser.
	 * @return      void
	 */
	protected function set_version( $version ) {
		$this->version = preg_replace( '/[^0-9,.,a-z,A-Z-]/', '', $version );
	}


	/**
	 * Whether or not the browser is from AOL
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      bool $this->is_aol Whether or not the browser is from AOL
	 */
	public function is_aol() {
		return $this->is_aol;
	}


	/**
	 * Set the browser to be from AOL
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @param       bool $is_aol Whether or not the browser is from AOL.
	 * @return      void
	 */
	protected function set_aol( $is_aol ) {
		$this->is_aol = $is_aol;
	}


	/**
	 * The version of AOL
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      string $this->aol_version The version of AOL
	 */
	public function get_aol_version() {
		return $this->aol_version;
	}


	/**
	 * Set the version of AOL
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @param       string $version The version of AOL.
	 * @return      void
	 */
	protected function set_aol_version( $version ) {
		$this->aol_version = preg_replace( '/[^0-9,.,a-z,A-Z]/', '', $version );
	}


	/**
	 * Whether or not the browser is a Facebook request
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      string $this->is_facebook Whether or not the browser is a Facebook request
	 */
	public function is_facebook() {
		return $this->is_facebook;
	}


	/**
	 * Set the browser to be a Facebook request
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @param       bool $is_facebook Whether or not the browser is a Facebook request.
	 * @return      void
	 */
	protected function set_facebook( $is_facebook = true ) {
		$this->is_facebook = $is_facebook;
	}


	/**
	 * Whether or not the browser is from a mobile device
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      bool $this->is_mobile Whether or not the browser is from a mobile device
	 */
	public function is_mobile() {
		return $this->is_mobile;
	}


	/**
	 * Whether or not the browser is from a tablet
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      bool $this->is_tablet Whether or not the browser is from a tablet
	 */
	public function is_tablet() {
		return $this->is_tablet;
	}


	/**
	 * Set the browser to be from a tablet
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @param       bool $is_tablet Whether or not the browser is from a tablet.
	 * @return      void
	 */
	protected function set_tablet( $is_tablet = true ) {
		$this->is_tablet = $is_tablet;
	}


	/**
	 * Set the browser to be mobile
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @param       bool $value Whether or not the browser is a mobile browser.
	 * @return      void
	 */
	protected function set_mobile( $value = true ) {
		$this->is_mobile = $value;
	}


	/**
	 * Whether or not the browser is a bot request
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      bool $this->is_bot Whether or not the browser is a bot request
	 */
	public function is_bot() {
		return $this->is_bot;
	}


	/**
	 * Set the browser to be a bot request
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @param       bool $is_bot Whether or not the browser is a bot request.
	 * @return      void
	 */
	protected function set_bot( $is_bot = true ) {
		$this->is_bot = $is_bot;
	}


	/**
	 * Get the user agent from the HTTP header
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      string $this->agent The user agent from the HTTP header.
	 */
	public function get_useragent() {
		return $this->agent;
	}


	/**
	 * Set the user agent value
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       string $agent_string The value for the user agent.
	 * @return      void
	 */
	public function set_useragent( $agent_string ) {
		$this->reset();
		$this->agent = $agent_string;
		$this->determine();
	}


	/**
	 * Used to determine if the browser is actually chromeframe
	 *
	 * @access      public
	 * @since       1.7.0
	 * @return      bool Whether the browser is using chromeframe
	 */
	public function is_chrome_frame() {
		return strpos( $this->agent, 'chromeframe' ) !== false;
	}


	/**
	 * Returns a formatted string with a summary of the details of the browser.
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      string The formatted string with a summary of the browser
	 */
	public function __toString() {
		$string = sprintf(
			"Browser Name:                 %s\n" .
			"Browser Version:              %s\n" .
			"User Agent String:            %s\n" .
			'Platform:                     %s',
			$this->get_browser(),
			$this->get_version(),
			$this->get_useragent(),
			$this->get_platform()
		);

		return $string;
	}


	/**
	 * Determine what the browser and platform are in use
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      void
	 */
	protected function determine() {
		$this->check_platform();
		$this->check_browsers();
		$this->check_for_aol();
	}


	/**
	 * Determine the browser type
	 *
	 * NOTES:
	 * (1) Opera must be checked before FireFox due to the odd
	 *     user agents used in some older versions of Opera.
	 * (2) WebTV is strapped onto Internet Explorer so we must
	 *     check for WebTV before IE.
	 * (3) (deprecated) Galeon is based on Firefox and needs to be
	 *     tested before Firefox is tested.
	 * (4) OmniWeb is based on Safari so OmniWeb check must occur
	 *     before Safari.
	 * (5) Netscape 9+ is based on Firefox so Netscape checks
	 *     before FireFox are necessary.
	 * (6) Vivaldi is UA contains both Firefox and Chrome so Vivaldi checks
	 *     before Firefox and Chrome.
	 * (7) Mozilla is such an open standard that it must be checked last.
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser was detected
	 */
	protected function check_browsers() {
		return (
			// Check edge cases.
			$this->check_browser_webtv() ||
			$this->check_browser_brave() ||
			$this->check_browser_ucbrowser() ||
			$this->check_browser_edge() ||
			$this->check_browser_internet_explorer() ||
			$this->check_browser_opera() ||
			$this->check_browser_galeon() ||
			$this->check_browser_netscape_navigator_9plus() ||
			$this->check_browser_vivaldi() ||
			$this->check_browser_yandex() ||
			$this->check_browser_palemoon() ||
			$this->check_browser_firefox() ||
			$this->check_browser_chrome() ||
			$this->check_browser_omniweb() ||

			// Common mobile.
			$this->check_browser_android() ||
			$this->check_browser_ipad() ||
			$this->check_browser_ipod() ||
			$this->check_browser_iphone() ||
			$this->check_browser_blackberry() ||
			$this->check_browser_nokia() ||

			// Common bots.
			$this->check_browser_googlebot() ||
			$this->check_browser_msn_bot() ||
			$this->check_browser_bing_bot() ||
			$this->check_browser_slurp() ||

			// Yandex bots.
			$this->check_browser_yandex_bot() ||
			$this->check_browser_yandex_bot_image_resizer() ||
			$this->check_browser_yandex_bot_blogs() ||
			$this->check_browser_yandex_bot_catalog() ||
			$this->check_browser_yandex_bot_direct() ||
			$this->check_browser_yandex_bot_favicons() ||
			$this->check_browser_yandex_bot_images() ||
			$this->check_browser_yandex_bot_media() ||
			$this->check_browser_yandex_bot_metrika() ||
			$this->check_browser_yandex_bot_news() ||
			$this->check_browser_yandex_bot_video() ||
			$this->check_browser_yandex_bot_webmaster() ||

			// Check for facebook external hit when loading URL.
			$this->check_browser_facebook() ||

			// WebKit base check (post mobile and others).
			$this->check_browser_samsung() ||
			$this->check_browser_silk() ||
			$this->check_browser_safari() ||

			// Everyone else.
			$this->check_browser_netpositive() ||
			$this->check_browser_firebird() ||
			$this->check_browser_konqueror() ||
			$this->check_browser_icab() ||
			$this->check_browser_phoenix() ||
			$this->check_browser_amaya() ||
			$this->check_browser_lynx() ||
			$this->check_browser_shiretoko() ||
			$this->check_browser_icecat() ||
			$this->check_browser_iceweasel() ||
			$this->check_browser_w3c_validator() ||
			$this->check_browser_curl() ||
			$this->check_browser_wget() ||
			$this->check_browser_playstation() ||
			$this->check_browser_iframely() ||
			$this->check_browser_cocoa() ||
			$this->check_browser_mozilla()
		);
	}


	/**
	 * Determine if the user is using an AOL user agent
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is from AOL
	 */
	protected function check_for_aol() {
		$this->set_aol( false );
		$this->set_aol_version( self::VERSION_UNKNOWN );

		if ( stripos( $this->agent, 'aol' ) !== false ) {
			$version = explode( ' ', stristr( $this->agent, 'AOL' ) );

			if ( isset( $version[1] ) ) {
				$this->set_aol( true );
				$this->set_aol_version( preg_replace( '/[^0-9\.a-z]/i', '', $version[1] ) );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is Amaya or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Amaya
	 */
	protected function check_browser_amaya() {
		if ( stripos( $this->agent, 'amaya' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'Amaya' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( $version[0] );
				$this->set_browser( self::BROWSER_AMAYA );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is Android or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Android
	 */
	protected function check_browser_android() {
		if ( stripos( $this->agent, 'Android' ) !== false ) {
			$result = explode( ' ', stristr( $this->agent, 'Android' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( $version[0] );
			} else {
				$this->set_version( self::VERSION_UNKNOWN );
			}

			if ( stripos( $this->agent, 'Mobile' ) !== false ) {
				$this->set_mobile( true );
			} else {
				$this->set_tablet( true );
			}

			$this->set_browser( self::BROWSER_ANDROID );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the user is using a BlackBerry
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is the BlackBerry browser
	 */
	protected function check_browser_blackberry() {
		if ( stripos( $this->agent, 'blackberry' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'BlackBerry' ) );

			if ( $result[1] ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( $version[0] );
				$this->browser_name = self::BROWSER_BLACKBERRY;
				$this->set_mobile( true );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is the Bing Bot or not
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is the Bing Bot, false otherwise
	 */
	protected function check_browser_bing_bot() {
		if ( stripos( $this->agent, 'bingbot' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'bingbot' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( str_replace( ';', '', $version[0] ) );
				$this->browser_name = self::BROWSER_BING_BOT;
				$this->set_bot( true );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is Brave or not
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is Brave, false otherwise
	 */
	protected function check_browser_brave() {
		if ( stripos( $this->agent, 'Brave/' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'Brave' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( $version[0] );
				$this->set_browser( self::BROWSER_BRAVE );

				return true;
			}
		} elseif ( stripos( $this->agent, ' Brave ' ) !== false ) {
			$this->set_browser( self::BROWSER_BRAVE );
			// This version of the UA did not ship with a version marker.
			$this->set_version( '' );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is Chrome or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Chrome
	 */
	protected function check_browser_chrome() {
		if ( stripos( $this->agent, 'Chrome' ) !== false ) {
			$result = preg_split( '/[\/;]+/', stristr( $this->agent, 'Chrome' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( $version[0] );
				$this->set_browser( self::BROWSER_CHROME );

				if ( stripos( $this->agent, 'Android' ) !== false ) {
					if ( stripos( $this->agent, 'Mobile' ) !== false ) {
						$this->set_mobile( true );
					} else {
						$this->set_tablet( true );
					}
				}

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is Cocoa Rest Client or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Cocoa Rest Client
	 */
	protected function check_browser_cocoa() {
		if ( stripos( $this->agent, 'CocoaRestClient' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'CocoaRestClient' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );
				$this->set_version( $version[0] );
			} else {
				$this->set_version( self::VERSION_UNKNOWN );
			}
			$this->set_browser( self::BROWSER_COCOA );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is cURL or not
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is cURL otherwise false
	 */
	protected function check_browser_curl() {
		if ( strpos( $this->agent, 'curl' ) === 0 ) {
			$result = explode( '/', stristr( $this->agent, 'curl' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );
				$this->set_version( $version[0] );
				$this->set_browser( self::BROWSER_CURL );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is Edge or not
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is Edge, false otherwise
	 */
	protected function check_browser_edge() {
		$name = ( stripos( $this->agent, 'Edge/' ) !== false ? 'Edge' : ( ( stripos( $this->agent, 'Edg/' ) !== false || stripos( $this->agent, 'EdgA/' ) !== false ) ? 'Edg' : false ) );

		if ( $name ) {
			$result = explode( '/', stristr( $this->agent, $name ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( $version[0] );
				$this->set_browser( self::BROWSER_EDGE );

				if ( stripos( $this->agent, 'Windows Phone' ) !== false || stripos( $this->agent, 'Android' ) !== false ) {
					$this->set_mobile( true );
				}

				return true;
			}
		}

		return false;
	}


	/**
	 * Detect if URL is loaded from FacebookExternalHit
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if it detects FacebookExternalHit otherwise false
	 */
	protected function check_browser_facebook() {
		if ( stristr( $this->agent, 'FacebookExternalHit' ) ) {
			$this->set_bot( true );
			$this->set_facebook( true );

			return true;
		}

		return false;
	}


	/**
	 * Detect if URL is being loaded from internal Facebook browser
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if it detects internal Facebook browser otherwise false
	 */
	protected function check_browser_facebook_ios() {
		if ( stristr( $this->agent, 'FBIOS' ) ) {
			$this->set_facebook( true );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is Firebird or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Firebird
	 */
	protected function check_browser_firebird() {
		if ( stripos( $this->agent, 'Firebird' ) !== false ) {
			$version = explode( '/', stristr( $this->agent, 'Firebird' ) );

			if ( isset( $version[1] ) ) {
				$this->set_version( $version[1] );
				$this->set_browser( self::BROWSER_FIREBIRD );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is Firefox or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Firefox
	 */
	protected function check_browser_firefox() {
		if ( stripos( $this->agent, 'safari' ) === false ) {
			if ( preg_match( '/Firefox[\/ \(]([^ ;\)]+)/i', $this->agent, $matches ) ) {
				$this->set_version( $matches[1] );
				$this->set_browser( self::BROWSER_FIREFOX );

				if ( stripos( $this->agent, 'Android' ) !== false || stripos( $this->agent, 'iPhone' ) !== false ) {
					if ( stripos( $this->agent, 'Mobile' ) !== false || stripos( $this->agent, 'Tablet' ) !== false ) {
						$this->set_mobile( true );
					} else {
						$this->set_tablet( true );
					}
				}

				return true;
			} elseif ( preg_match( '/Firefox([0-9a-zA-Z\.]+)/i', $this->agent, $matches ) ) {
				$this->set_version( $matches[1] );
				$this->set_browser( self::BROWSER_FIREFOX );

				return true;
			} elseif ( preg_match( '/Firefox$/i', $this->agent, $matches ) ) {
				$this->set_version( '' );
				$this->set_browser( self::BROWSER_FIREFOX );

				return true;
			}
		} elseif ( preg_match( '/FxiOS[\/ \(]([^ ;\)]+)/i', $this->agent, $matches ) ) {
			$this->set_version( $matches[1] );
			$this->set_browser( self::BROWSER_FIREFOX );

			if ( stripos( $this->agent, 'Android' ) !== false || stripos( $this->agent, 'iPhone' ) !== false ) {
				if ( stripos( $this->agent, 'Mobile' ) !== false || stripos( $this->agent, 'Tablet' ) !== false ) {
					$this->set_mobile( true );
				} else {
					$this->set_tablet( true );
				}
			}

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is Galeon or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Galeon
	 */
	protected function check_browser_galeon() {
		if ( stripos( $this->agent, 'galeon' ) !== false ) {
			$result  = explode( ' ', stristr( $this->agent, 'galeon' ) );
			$version = explode( '/', $result[0] );

			if ( isset( $result[1] ) ) {
				$this->set_version( $version[1] );
				$this->set_browser( self::BROWSER_GALEON );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is the GoogleBot or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is the GoogleBot
	 */
	protected function check_browser_googlebot() {
		if ( stripos( $this->agent, 'googlebot' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'googlebot' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( str_replace( ';', '', $version[0] ) );
				$this->browser_name = self::BROWSER_GOOGLEBOT;
				$this->set_bot( true );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is iCab or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is iCab
	 */
	protected function check_browser_icab() {
		if ( stripos( $this->agent, 'icab' ) !== false ) {
			$version = explode( ' ', stristr( str_replace( '/', ' ', $this->agent ), 'icab' ) );

			if ( isset( $version[1] ) ) {
				$this->set_version( $version[1] );
				$this->set_browser( self::BROWSER_ICAB );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is Ice Cat or not (http://en.wikipedia.org/wiki/GNU_IceCat )
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Ice Cat
	 */
	protected function check_browser_icecat() {
		if ( stripos( $this->agent, 'Mozilla' ) !== false && preg_match( '/IceCat\/([^ ]*)/i', $this->agent, $matches ) ) {
			$this->set_version( $matches[1] );
			$this->set_browser( self::BROWSER_ICECAT );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is Iceweasel or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Iceweasel
	 */
	protected function check_browser_iceweasel() {
		if ( stripos( $this->agent, 'Iceweasel' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'Iceweasel' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( $version[0] );
				$this->set_browser( self::BROWSER_ICEWEASEL );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is Iframely or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Iframely
	 */
	protected function check_browser_iframely() {
		if ( stripos( $this->agent, 'Iframely' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'Iframely' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );
				$this->set_version( $version[0] );
			} else {
				$this->set_version( self::VERSION_UNKNOWN );
			}

			$this->set_browser( self::BROWSER_IFRAMELY );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is Internet Explorer or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Internet Explorer
	 */
	protected function check_browser_internet_explorer() {
		if ( stripos( $this->agent, 'Trident/7.0; rv:11.0' ) !== false ) {
			// Test for IE11.
			$this->set_browser( self::BROWSER_INTERNET_EXPLORER );
			$this->set_version( '11.0' );

			return true;
		} elseif ( stripos( $this->agent, 'microsoft internet explorer' ) !== false ) {
			// Test for v1 - v1.5 IE.
			$this->set_browser( self::BROWSER_INTERNET_EXPLORER );
			$this->set_version( '1.0' );

			$result = stristr( $this->agent, '/' );

			if ( preg_match( '/308|425|426|474|0b1/i', $result ) ) {
				$this->set_version( '1.5' );
			}

			return true;
		} elseif ( stripos( $this->agent, 'msie' ) !== false && stripos( $this->agent, 'opera' ) === false ) {
			// Test for versions > 1.5.
			if ( stripos( $this->agent, 'msnb' ) !== false ) {
				// See if the browser is the odd MSN Explorer.
				$result = explode( ' ', stristr( str_replace( ';', '; ', $this->agent ), 'MSN' ) );

				if ( isset( $result[1] ) ) {
					$this->set_browser( self::BROWSER_MSN );
					$this->set_version( str_replace( array( '( ', ' )', ';' ), '', $result[1] ) );

					return true;
				}
			}

			$result = explode( ' ', stristr( str_replace( ';', '; ', $this->agent ), 'msie' ) );

			if ( isset( $result[1] ) ) {
				$this->set_browser( self::BROWSER_INTERNET_EXPLORER );
				$this->set_version( str_replace( array( '( ', ' )', ';' ), '', $result[1] ) );

				if ( stripos( $this->agent, 'IEMobile' ) !== false ) {
					$this->set_browser( self::BROWSER_POCKET_IE );
					$this->set_mobile( true );
				}
				return true;
			}
		} elseif ( stripos( $this->agent, 'trident' ) !== false ) {
			// Test for versions > IE 10.
			$this->set_browser( self::BROWSER_INTERNET_EXPLORER );

			$result = explode( 'rv:', $this->agent );

			if ( isset( $result[1] ) ) {
				$this->set_version( preg_replace( '/[^0-9.]+/', '', $result[1] ) );
				$this->agent = str_replace( array( 'Mozilla', 'Gecko' ), 'MSIE', $this->agent );
			}
		} elseif ( stripos( $this->agent, 'mspie' ) !== false || stripos( $this->agent, 'pocket' ) !== false ) {
			// Test for Pocket IE.
			$result = explode( ' ', stristr( $this->agent, 'mspie' ) );

			if ( isset( $result[1] ) ) {
				$this->set_platform( self::PLATFORM_WINDOWS_CE );
				$this->set_browser( self::BROWSER_POCKET_IE );
				$this->set_mobile( true );

				if ( stripos( $this->agent, 'mspie' ) !== false ) {
					$this->set_version( $result[1] );
				} else {
					$version = explode( '/', $this->agent );

					if ( isset( $version[1] ) ) {
						$this->set_version( $version[1] );
					}
				}

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is iPod or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is iPod
	 */
	protected function check_browser_ipad() {
		if ( stripos( $this->agent, 'iPad' ) !== false ) {
			$this->set_version( self::VERSION_UNKNOWN );
			$this->set_browser( self::BROWSER_IPOD );
			$this->get_safari_ios_version();
			$this->get_chrome_ios_version();
			$this->check_browser_facebook_ios();
			$this->set_mobile( true );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is iPhone or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is iPhone
	 */
	protected function check_browser_iphone() {
		if ( stripos( $this->agent, 'iPhone' ) !== false ) {
			$this->set_version( self::VERSION_UNKNOWN );
			$this->set_browser( self::BROWSER_IPHONE );
			$this->get_safari_ios_version();
			$this->get_chrome_ios_version();
			$this->check_browser_facebook_ios();
			$this->set_mobile( true );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is iPod or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is iPod
	 */
	protected function check_browser_ipod() {
		if ( stripos( $this->agent, 'iPod' ) !== false ) {
			$this->set_version( self::VERSION_UNKNOWN );
			$this->set_browser( self::BROWSER_IPOD );
			$this->get_safari_ios_version();
			$this->get_chrome_ios_version();
			$this->check_browser_facebook_ios();
			$this->set_mobile( true );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is Konqueror or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Konqueror
	 */
	protected function check_browser_konqueror() {
		if ( stripos( $this->agent, 'Konqueror' ) !== false ) {
			$result  = explode( ' ', stristr( $this->agent, 'Konqueror' ) );
			$version = explode( '/', $result[0] );

			if ( isset( $version[1] ) ) {
				$this->set_version( $version[1] );
				$this->set_browser( self::BROWSER_KONQUEROR );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is Lynx or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Lynx
	 */
	protected function check_browser_lynx() {
		if ( stripos( $this->agent, 'lynx' ) !== false ) {
			$result  = explode( '/', stristr( $this->agent, 'Lynx' ) );
			$version = explode( ' ', ( isset( $result[1] ) ? $result[1] : '' ) );

			$this->set_version( $version[0] );
			$this->set_browser( self::BROWSER_LYNX );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is Mozilla or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Mozilla
	 */
	protected function check_browser_mozilla() {
		if ( stripos( $this->agent, 'mozilla' ) !== false && preg_match( '/rv:[0-9].[0-9][a-b]?/i', $this->agent ) && stripos( $this->agent, 'netscape' ) === false ) {
			$version = explode( ' ', stristr( $this->agent, 'rv:' ) );

			preg_match( '/rv:[0-9].[0-9][a-b]?/i', $this->agent, $version );

			$this->set_version( str_replace( 'rv:', '', $version[0] ) );
			$this->set_browser( self::BROWSER_MOZILLA );

			return true;
		} elseif ( stripos( $this->agent, 'mozilla' ) !== false && preg_match( '/rv:[0-9]\.[0-9]/i', $this->agent ) && stripos( $this->agent, 'netscape' ) === false ) {
			$version = explode( '', stristr( $this->agent, 'rv:' ) );

			$this->set_version( str_replace( 'rv:', '', $version[0] ) );
			$this->set_browser( self::BROWSER_MOZILLA );

			return true;
		} elseif ( stripos( $this->agent, 'mozilla' ) !== false && preg_match( '/mozilla\/([^ ]*)/i', $this->agent, $matches ) && stripos( $this->agent, 'netscape' ) === false ) {
			$this->set_version( $matches[1] );
			$this->set_browser( self::BROWSER_MOZILLA );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is the MSN Bot or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is the MSN Bot
	 */
	protected function check_browser_msn_bot() {
		if ( stripos( $this->agent, 'msnbot' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'msnbot' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( str_replace( ';', '', $version[0] ) );
				$this->browser_name = self::BROWSER_MSN_BOT;
				$this->set_bot( true );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is NetPositive or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is NetPositive
	 */
	protected function check_browser_netpositive() {
		if ( stripos( $this->agent, 'NetPositive' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'NetPositive' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( str_replace( array( '( ', ' )', ';' ), '', $version[0] ) );
				$this->set_browser( self::BROWSER_NETPOSITIVE );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is Netscape Navigator 9+ or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Netscape Navigator 9+
	 */
	protected function check_browser_netscape_navigator_9plus() {
		if ( stripos( $this->agent, 'Firefox' ) !== false && preg_match( '/Navigator\/([^ ]*)/i', $this->agent, $matches ) ) {
			$this->set_version( $matches[1] );
			$this->set_browser( self::BROWSER_NETSCAPE_NAVIGATOR );

			return true;
		} elseif ( stripos( $this->agent, 'Firefox' ) === false && preg_match( '/Netscape6?\/([^ ]*)/i', $this->agent, $matches ) ) {
			$this->set_version( $matches[1] );
			$this->set_browser( self::BROWSER_NETSCAPE_NAVIGATOR );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is Nokia or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Nokia
	 */
	protected function check_browser_nokia() {
		if ( preg_match( '/Nokia([^\/]+)\/([^ SP]+)/i', $this->agent, $matches ) ) {
			$this->set_version( $matches[2] );

			if ( stripos( $this->agent, 'Series60' ) !== false || strpos( $this->agent, 'S60' ) !== false ) {
				$this->set_browser( self::BROWSER_NOKIA_S60 );
			} else {
				$this->set_browser( self::BROWSER_NOKIA );
			}

			$this->set_mobile( true );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is OmniWeb or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is OmniWeb
	 */
	protected function check_browser_omniweb() {
		if ( stripos( $this->agent, 'omniweb' ) !== false ) {
			$result  = explode( '/', stristr( $this->agent, 'omniweb' ) );
			$version = explode( ' ', isset( $result[1] ) ? $result[1] : '' );

			$this->set_version( $version[0] );
			$this->set_browser( self::BROWSER_OMNIWEB );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is Opera or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Opera
	 */
	protected function check_browser_opera() {
		if ( stripos( $this->agent, 'opera mini' ) !== false ) {
			$result = stristr( $this->agent, 'opera mini' );

			if ( preg_match( '/\//', $result ) ) {
				$result = explode( '/', $result );

				if ( isset( $result[1] ) ) {
					$version = explode( ' ', $result[1] );

					$this->set_version( $version[0] );
				}
			} else {
				$version = explode( ' ', stristr( $result, 'opera mini' ) );

				$this->set_version( $version[1] );
			}

			$this->browser_name = self::BROWSER_OPERA_MINI;
			$this->set_mobile( true );

			return true;
		} elseif ( stripos( $this->agent, 'opera' ) !== false ) {
			$result = stristr( $this->agent, 'opera' );

			if ( preg_match( '/Version\/(10.*)$/', $result, $matches ) ) {
				$this->set_version( $matches[1] );
			} elseif ( preg_match( '/\//', $result ) ) {
				$result = explode( '/', str_replace( '( ', ' ', $result ) );

				if ( isset( $result[1] ) ) {
					$version = explode( ' ', $result[1] );

					$this->set_version( $version[0] );
				}
			} else {
				$version = explode( ' ', stristr( $result, 'opera' ) );

				$this->set_version( isset( $version[1] ) ? $version[1] : '' );
			}

			if ( stripos( $this->agent, 'Opera Mobi' ) !== false ) {
				$this->set_mobile( true );
			}
		} elseif ( stripos( $this->agent, 'OPR' ) !== false ) {
			$result = stristr( $this->agent, 'OPR' );

			if ( preg_match( '/\//', $result ) ) {
				$result = explode( '/', str_replace( '( ', ' ', $result ) );

				if ( isset( $result[1] ) ) {
					$version = explode( ' ', $result[1] );

					$this->set_version( $version[0] );
				}
			}

			if ( stripos( $this->agent, 'Mobile' ) !== false ) {
				$this->set_mobile( true );
			}

			$this->browser_name = self::BROWSER_OPERA;

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is Phoenix or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Phoenix
	 */
	protected function check_browser_phoenix() {
		if ( stripos( $this->agent, 'Phoenix' ) !== false ) {
			$version = explode( '/', stristr( $this->agent, 'Phoenix' ) );

			if ( isset( $version[1] ) ) {
				$this->set_version( $version[1] );
				$this->set_browser( self::BROWSER_PHOENIX );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is Palemoon or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool True if the browser is Palemoon otherwise false
	 */
	protected function check_browser_palemoon() {
		if ( stripos( $this->agent, 'safari' ) === false ) {
			if ( preg_match( '/Palemoon[\/ \(]([^ ;\)]+)/i', $this->agent, $matches ) ) {
				$this->set_version( $matches[1] );
				$this->set_browser( self::BROWSER_PALEMOON );

				return true;
			} elseif ( preg_match( '/Palemoon([0-9a-zA-Z\.]+)/i', $this->agent, $matches ) ) {
				$this->set_version( $matches[1] );
				$this->set_browser( self::BROWSER_PALEMOON );

				return true;
			} elseif ( preg_match( '/Palemoon/i', $this->agent, $matches ) ) {
				$this->set_version( '' );
				$this->set_browser( self::BROWSER_PALEMOON );

				return true;
			}
		}
		return false;
	}


	/**
	 * Determine if the browser is a PlayStation
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is PlayStation otherwise false
	 */
	protected function check_browser_playstation() {
		if ( stripos( $this->agent, 'PlayStation ' ) !== false ) {
			$result = explode( ' ', stristr( $this->agent, 'PlayStation ' ) );

			$this->set_browser( self::PLATFORM_PLAYSTATION );

			if ( isset( $result[0] ) ) {
				$version = explode( ' )', $result[2] );
				$this->set_version( $version[0] );

				if ( stripos( $this->agent, 'Portable )' ) !== false || stripos( $this->agent, 'Vita' ) !== false ) {
					$this->set_mobile( true );
				}

				return true;
			}
		}
		return false;
	}


	/**
	 * Determine if the browser is Safari or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Safari
	 */
	protected function check_browser_safari() {
		if ( stripos( $this->agent, 'Safari' ) !== false && stripos( $this->agent, 'iPhone' ) === false && stripos( $this->agent, 'iPod' ) === false ) {
			$result = explode( '/', stristr( $this->agent, 'Version' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( $version[0] );
			} else {
				$this->set_version( self::VERSION_UNKNOWN );
			}

			$this->set_browser( self::BROWSER_SAFARI );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is from Samsung or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is from Samsung
	 */
	protected function check_browser_samsung() {
		if ( stripos( $this->agent, 'SamsungBrowser' ) !== false ) {

			$result = explode( '/', stristr( $this->agent, 'SamsungBrowser' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );
				$this->set_version( $version[0] );
			} else {
				$this->set_version( self::VERSION_UNKNOWN );
			}

			$this->set_browser( self::BROWSER_SAMSUNG_INTERNET );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is Shiretoko or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is Shiretoko
	 */
	protected function check_browser_shiretoko() {
		if ( stripos( $this->agent, 'Mozilla' ) !== false && preg_match( '/Shiretoko\/([^ ]*)/i', $this->agent, $matches ) ) {
			$this->set_version( $matches[1] );
			$this->set_browser( self::BROWSER_SHIRETOKO );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is Silk or not
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool Whether or not the browser is Silk
	 */
	protected function check_browser_silk() {
		if ( stripos( $this->agent, 'Silk' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'Silk' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );
				$this->set_version( $version[0] );
			} else {
				$this->set_version( self::VERSION_UNKNOWN );
			}

			$this->set_browser( self::BROWSER_SILK );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is the Yahoo! Slurp Robot or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is the Yahoo! Slurp Robot
	 */
	protected function check_browser_slurp() {
		if ( stripos( $this->agent, 'slurp' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'Slurp' ) );

			if ( $result[1] ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( $version[0] );
				$this->browser_name = self::BROWSER_SLURP;
				$this->set_bot( true );
				$this->set_mobile( false );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is UCBrowser or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool True if the browser is UCBrowser otherwise false
	 */
	protected function check_browser_ucbrowser() {
		if ( preg_match( '/UC ?Browser\/?([\d\.]+)/', $this->agent, $matches ) ) {
			if ( isset( $matches[1] ) ) {
				$this->set_version( $matches[1] );
			}

			if ( stripos( $this->agent, 'Mobile' ) !== false ) {
				$this->set_mobile( true );
			} else {
				$this->set_tablet( true );
			}

			$this->set_browser( self::BROWSER_UCBROWSER );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is Vivaldi
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is Vivaldi otherwise false
	 */
	protected function check_browser_vivaldi() {
		if ( stripos( $this->agent, 'Vivaldi' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'Vivaldi' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );
				$this->set_version( $version[0] );
				$this->set_browser( self::BROWSER_VIVALDI );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is the W3C Validator or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is the W3C Validator
	 */
	protected function check_browser_w3c_validator() {
		if ( stripos( $this->agent, 'W3C-checklink' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'W3C-checklink' ) );

			if ( $result[1] ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( $version[0] );
				$this->browser_name = self::BROWSER_W3C_VALIDATOR;

				return true;
			}
		} elseif ( stripos( $this->agent, 'W3C_Validator' ) !== false ) {
			// Some of the Validator versions do not delineate with a slash - add it back in.
			$ua     = str_replace( 'W3C_Validator ', 'W3C_Validator/', $this->agent );
			$result = explode( '/', stristr( $ua, 'W3C_Validator' ) );

			if ( $result[1] ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( $version[0] );
				$this->browser_name = self::BROWSER_W3C_VALIDATOR;

				return true;
			}
		} elseif ( stripos( $this->agent, 'W3C-mobileOK' ) !== false ) {
			$this->browser_name = self::BROWSER_W3C_VALIDATOR;
			$this->set_mobile( true );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is WebTv or not
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      bool Whether or not the browser is WebTv
	 */
	protected function check_browser_webtv() {
		if ( stripos( $this->agent, 'webtv' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'webtv' ) );

			if ( $result[1] ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( $version[0] );
				$this->set_browser( self::BROWSER_WEBTV );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is Wget or not
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is Wget otherwise false
	 */
	protected function check_browser_wget() {
		if ( preg_match( '!^Wget/([^ ]+)!i', $this->agent, $result ) ) {
			$this->set_version( $result[1] );
			$this->set_browser( self::BROWSER_WGET );

			return true;
		}

		return false;
	}


	/**
	 * Determine if the browser is Yandex
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is Yandex otherwise false
	 */
	protected function check_browser_yandex() {
		if ( stripos( $this->agent, 'YaBrowser' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'YaBrowser' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );
				$this->set_version( $version[0] );
				$this->set_browser( self::BROWSER_YANDEX );

				if ( stripos( $this->agent, 'iPad' ) !== false ) {
					$this->set_tablet( true );
				} elseif ( stripos( $this->agent, 'Mobile' ) !== false ) {
					$this->set_mobile( true );
				} elseif ( stripos( $this->agent, 'Android' ) !== false ) {
					$this->set_tablet( true );
				}

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is the YandexBot or not
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is the YandexBot, false otherwise
	 */
	protected function check_browser_yandex_bot() {
		if ( stripos( $this->agent, 'YandexBot' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'YandexBot' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( str_replace( ';', '', $version[0] ) );
				$this->browser_name = self::BROWSER_YANDEX_BOT;
				$this->set_bot( true );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is the Yandex Blogs Bot or not
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is the Yandex Blogs Bot, false otherwise
	 */
	protected function check_browser_yandex_bot_blogs() {
		if ( stripos( $this->agent, 'YandexBlogs' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'YandexBlogs' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( str_replace( ';', '', $version[0] ) );
				$this->browser_name = self::BROWSER_YANDEX_BOT_BLOGS;
				$this->set_bot( true );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is the Yandex Catalog Bot or not
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is the Yandex Catalog Bot, false otherwise
	 */
	protected function check_browser_yandex_bot_catalog() {
		if ( stripos( $this->agent, 'YandexCatalog' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'YandexCatalog' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( str_replace( ';', '', $version[0] ) );
				$this->browser_name = self::BROWSER_YANDEX_BOT_CATALOG;
				$this->set_bot( true );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is the Yandex Direct Bot or not
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is the Yandex Direct Bot, false otherwise
	 */
	protected function check_browser_yandex_bot_direct() {
		if ( stripos( $this->agent, 'YandexDirect' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'YandexDirect' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( str_replace( ';', '', $version[0] ) );
				$this->browser_name = self::BROWSER_YANDEX_BOT_DIRECT;
				$this->set_bot( true );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is the Yandex Favicons Bot or not
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is the Yandex Favicons Bot, false otherwise
	 */
	protected function check_browser_yandex_bot_favicons() {
		if ( stripos( $this->agent, 'YandexFavicons' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'YandexFavicons' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( str_replace( ';', '', $version[0] ) );
				$this->browser_name = self::BROWSER_YANDEX_BOT_FAVICONS;
				$this->set_bot( true );

				return true;
			}
		}
		return false;
	}


	/**
	 * Determine if the browser is the Yandex ImageResizer Bot or not
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is the Yandex ImageResizer Bot, false otherwise
	 */
	protected function check_browser_yandex_bot_image_resizer() {
		if ( stripos( $this->agent, 'YandexImageResizer' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'YandexImageResizer' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( str_replace( ';', '', $version[0] ) );
				$this->browser_name = self::BROWSER_YANDEX_BOT_IMAGERESIZER;
				$this->set_bot( true );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is the Yandex Images Bot or not
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is the Yandex Images Bot, false otherwise
	 */
	protected function check_browser_yandex_bot_images() {
		if ( stripos( $this->agent, 'YandexImages' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'YandexImages' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( str_replace( ';', '', $version[0] ) );
				$this->browser_name = self::BROWSER_YANDEX_BOT_IMAGES;
				$this->set_bot( true );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is the Yandex Media Bot or not
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is the Yandex Media Bot, false otherwise
	 */
	protected function check_browser_yandex_bot_media() {
		if ( stripos( $this->agent, 'YandexMedia' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'YandexMedia' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( str_replace( ';', '', $version[0] ) );
				$this->browser_name = self::BROWSER_YANDEX_BOT_MEDIA;
				$this->set_bot( true );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is the Yandex Metrika Bot or not
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is the Yandex Metrika Bot, false otherwise
	 */
	protected function check_browser_yandex_bot_metrika() {
		if ( stripos( $this->agent, 'YandexMetrika' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'YandexMetrika' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( str_replace( ';', '', $version[0] ) );
				$this->browser_name = self::BROWSER_YANDEX_BOT_METRIKA;
				$this->set_bot( true );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is the Yandex News Bot or not
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is the Yandex News Bot, false otherwise
	 */
	protected function check_browser_yandex_bot_news() {
		if ( stripos( $this->agent, 'YandexNews' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'YandexNews' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( str_replace( ';', '', $version[0] ) );
				$this->browser_name = self::BROWSER_YANDEX_BOT_NEWS;
				$this->set_bot( true );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is the Yandex Video Bot or not
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is the Yandex Video Bot, false otherwise
	 */
	protected function check_browser_yandex_bot_video() {
		if ( stripos( $this->agent, 'YandexVideo' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'YandexVideo' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( str_replace( ';', '', $version[0] ) );
				$this->browser_name = self::BROWSER_YANDEX_BOT_VIDEO;
				$this->set_bot( true );

				return true;
			}
		}

		return false;
	}


	/**
	 * Determine if the browser is the Yandex Webmaster Bot or not
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if the browser is the Yandex Webmaster Bot, false otherwise
	 */
	protected function check_browser_yandex_bot_webmaster() {
		if ( stripos( $this->agent, 'YandexWebmaster' ) !== false ) {
			$result = explode( '/', stristr( $this->agent, 'YandexWebmaster' ) );

			if ( isset( $result[1] ) ) {
				$version = explode( ' ', $result[1] );

				$this->set_version( str_replace( ';', '', $version[0] ) );
				$this->browser_name = self::BROWSER_YANDEX_BOT_WEBMASTER;
				$this->set_bot( true );

				return true;
			}
		}

		return false;
	}


	/**
	 * Detect version for the Chrome browser on iOS devices
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if it detects the version correctly otherwise false
	 */
	protected function get_chrome_ios_version() {
		$result = explode( '/', stristr( $this->agent, 'CriOS' ) );

		if ( isset( $result[1] ) ) {
			$version = explode( ' ', $result[1] );
			$this->set_version( $version[0] );
			$this->set_browser( self::BROWSER_CHROME );

			return true;
		}

		return false;
	}


	/**
	 * Detect version for the Safari browser on iOS devices
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @return      bool True if it detects the version correctly otherwise false
	 */
	protected function get_safari_ios_version() {
		$result = explode( '/', stristr( $this->agent, 'Version' ) );

		if ( isset( $result[1] ) ) {
			$version = explode( ' ', $result[1] );
			$this->set_version( $version[0] );

			return true;
		}

		return false;
	}


	/**
	 * Determine the user's platform
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @return      void
	 */
	protected function check_platform() {
		if ( stripos( $this->agent, 'windows' ) !== false ) {
			$this->platform = self::PLATFORM_WINDOWS;
		} elseif ( stripos( $this->agent, 'iPad' ) !== false ) {
			$this->platform = self::PLATFORM_IPAD;
		} elseif ( stripos( $this->agent, 'iPod' ) !== false ) {
			$this->platform = self::PLATFORM_IPOD;
		} elseif ( stripos( $this->agent, 'iPhone' ) !== false ) {
			$this->platform = self::PLATFORM_IPHONE;
		} elseif ( stripos( $this->agent, 'mac' ) !== false ) {
			$this->platform = self::PLATFORM_APPLE;
		} elseif ( stripos( $this->agent, 'android' ) !== false ) {
			$this->platform = self::PLATFORM_ANDROID;
		} elseif ( stripos( $this->agent, 'silk' ) !== false ) {
			$this->platform = self::PLATFORM_FIRE_OS;
		} elseif ( stripos( $this->agent, 'linux' ) !== false && stripos( $this->agent, 'SMART-TV' ) !== false ) {
			$this->platform = self::PLATFORM_LINUX . '/' . self::PLATFORM_SMART_TV;
		} elseif ( stripos( $this->agent, 'linux' ) !== false ) {
			$this->platform = self::PLATFORM_LINUX;
		} elseif ( stripos( $this->agent, 'Nokia' ) !== false ) {
			$this->platform = self::PLATFORM_NOKIA;
		} elseif ( stripos( $this->agent, 'BlackBerry' ) !== false ) {
			$this->platform = self::PLATFORM_BLACKBERRY;
		} elseif ( stripos( $this->agent, 'FreeBSD' ) !== false ) {
			$this->platform = self::PLATFORM_FREEBSD;
		} elseif ( stripos( $this->agent, 'OpenBSD' ) !== false ) {
			$this->platform = self::PLATFORM_OPENBSD;
		} elseif ( stripos( $this->agent, 'NetBSD' ) !== false ) {
			$this->platform = self::PLATFORM_NETBSD;
		} elseif ( stripos( $this->agent, 'OpenSolaris' ) !== false ) {
			$this->platform = self::PLATFORM_OPENSOLARIS;
		} elseif ( stripos( $this->agent, 'SunOS' ) !== false ) {
			$this->platform = self::PLATFORM_SUNOS;
		} elseif ( stripos( $this->agent, 'OS\/2' ) !== false ) {
			$this->platform = self::PLATFORM_OS2;
		} elseif ( stripos( $this->agent, 'BeOS' ) !== false ) {
			$this->platform = self::PLATFORM_BEOS;
		} elseif ( stripos( $this->agent, 'win' ) !== false ) {
			$this->platform = self::PLATFORM_WINDOWS;
		} elseif ( stripos( $this->agent, 'Playstation' ) !== false ) {
			$this->_platform = self::PLATFORM_PLAYSTATION;
		} elseif ( stripos( $this->agent, 'Roku' ) !== false ) {
			$this->_platform = self::PLATFORM_ROKU;
		} elseif ( stripos( $this->agent, 'iOS' ) !== false ) {
			$this->_platform = self::PLATFORM_IPHONE . '/' . self::PLATFORM_IPAD;
		} elseif ( stripos( $this->agent, 'tvOS' ) !== false ) {
			$this->_platform = self::PLATFORM_APPLE_TV;
		} elseif ( stripos( $this->agent, 'curl' ) !== false ) {
			$this->_platform = self::PLATFORM_TERMINAL;
		} elseif ( stripos( $this->agent, 'CrOS' ) !== false ) {
			$this->_platform = self::PLATFORM_CHROME_OS;
		} elseif ( stripos( $this->agent, 'okhttp' ) !== false ) {
			$this->_platform = self::PLATFORM_JAVA_ANDROID;
		} elseif ( stripos( $this->agent, 'PostmanRuntime' ) !== false ) {
			$this->_platform = self::PLATFORM_POSTMAN;
		} elseif ( stripos( $this->agent, 'Iframely' ) !== false ) {
			$this->_platform = self::PLATFORM_IFRAMELY;
		}
	}
}
