<?php
/**
 * Tab Delimited File Iterator
 *
 * @package     Browser\Tests\class-tab-delimited-file-iterator
 * @since       3.0.0
 */

// phpcs:disable WordPress.WP.AlternativeFunctions

declare( strict_types = 1 );

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use PHPUnit\Framework\TestCase;

/**
 * Tab Delimited File Iterator
 *
 * @since       3.0.0
 */
class Tab_Delimited_File_Iterator implements Iterator {


	/**
	 * The file we are working with
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @var         string $file The file we are working with
	 */
	protected $file;


	/**
	 * The current iterator key
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @var         int $key The current iterator key
	 */
	protected $key = 0;


	/**
	 * The current iterator array
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @var         array $current The current iterator array
	 */
	protected $current;


	/**
	 * Get things started
	 *
	 * @access      public
	 * @since       3.0.0
	 * @param       string $file The file we are working with.
	 * @return      void
	 */
	public function __construct( $file ) {
		$this->file = fopen( $file, 'r' );
	}


	/**
	 * Break things down
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      void
	 */
	public function __destruct() {
		fclose( $this->file );
	}


	/**
	 * Rewind the file pointer
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      void
	 */
	public function rewind() {
		rewind( $this->file );

		$this->current = fgetcsv( $this->file, 0, "\t" );
		$this->key     = 0;
	}


	/**
	 * Check if the file is valid
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      bool True if file is valid, false otherwise
	 */
	public function valid() {
		return ! feof( $this->file );
	}


	/**
	 * Get the current key
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      int $key The current key
	 */
	public function key() {
		return $this->key;
	}


	/**
	 * Get the current iterator array
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      array The current iterator array
	 */
	public function current() {
		return $this->current;
	}


	/**
	 * Update the current iterator data
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      void
	 */
	public function next() {
		$this->current = fgetcsv( $this->file, 0, "\t" );
		$this->key++;
	}
}
