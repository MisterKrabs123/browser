<?php
/**
 * Unit tests for cURL/Wget
 *
 * Originally created by willemstuursma (https://github.com/willemstuursma)
 *
 * @package     Browser\Tests\Browser\CURL_Wget
 * @since       3.0.0
 */

declare( strict_types = 1 );

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use PHPUnit\Framework\TestCase;

require_once dirname( __FILE__ ) . '/class-tab-delimited-file-iterator.php';

/**
 * Unit tests for cURL/Wget
 *
 * @since       3.0.0
 */
final class Tests_Browser_CURL_Wget extends TestCase {


	/**
	 * Test cURL/Wget user agents
	 *
	 * @access      public
	 * @since       3.0.0
	 * @param       string $user_agent The user agent to test.
	 * @param       string $browser_name The name of the browser.
	 * @param       string $browser_version The version of the browser.
	 * @return      void
	 */
	public function test_curl_wget_user_agents( $user_agent, $browser_name, $browser_version ) {
		$browser = new Browser( $user_agent );

		$this->assertSame( $browser_name, $browser->get_browser() );
		$this->assertSame( $browser_version, $browser->get_version() );
	}


	/**
	 * Load the cURL/Wget user agents
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      object The cURL/Wget user agents
	 */
	public function user_agent_curl_wget_provider() {
		return array(
			array( 'curl/7.37.1', Browser::BROWSER_CURL, '7.37.1' ),
			array( 'Wget/1.16 (darwin14.0.0)', Browser::BROWSER_WGET, '1.16' ),
		);
	}
}
