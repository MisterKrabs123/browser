<?php
/**
 * Edge user tests
 *
 * @package     Browser\Tests\Browser\Edge
 * @since       3.0.0
 */

declare( strict_types = 1 );

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use PHPUnit\Framework\TestCase;

require_once dirname( __FILE__ ) . '/class-tab-delimited-file-iterator.php';

/**
 * Edge unit tests
 *
 * @since       3.0.0
 */
final class Tests_Browser_Edge extends TestCase {


	/**
	 * Test Edge user agent
	 *
	 * @access      public
	 * @since       3.0.0
	 * @param       string $user_agent The user agent to test.
	 * @param       string $browser_type The type of browser.
	 * @param       string $browser_name The name of the browser.
	 * @param       string $browser_version The version of the browser.
	 * @param       string $os_type The type of operating system associated with the browser.
	 * @param       string $os_name The name of the operating system associated with the browser.
	 * @param       string $os_version_name The version of the operating system.
	 * @param       string $os_version_number The version of the operating system.
	 * @return      void
	 */
	public function test_edge_user_agent( $user_agent, $browser_type, $browser_name, $browser_version, $os_type, $os_name, $os_version_name, $os_version_number ) {
		$browser = new Browser( $user_agent );

		$this->assertSame( $browser_name, $browser->get_browser() );
		$this->assertSame( $browser_version, $browser->get_version() );
	}


	/**
	 * Load the Edge user agents
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      object The Edge user agents
	 */
	public function user_agent_edge_provider() {
		return new Tab_Delimited_File_Iterator( dirname( __FILE__ ) . '/lists/edge.txt' );
	}
}
